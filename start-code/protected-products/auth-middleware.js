const {readFileSync} = require('node:fs');
const JWT = require('njwt');


function loadDb(file) {
    const dbPath = __dirname + '/' + file;
    const data = readFileSync(dbPath, 'utf-8');
    return JSON.parse(data);
}

function findUser(username, db) {
    return db.users.find(u => u.username === username);
}

function mkToken(username, db) {
    const claims = {
        iss: 'ribomation.se',
        sub: username
    };
    const token = JWT.create(claims, db.metaData.secretKey);
    const MINUTE = 60 * 1000;
    token.setExpiration(Math.floor(Date.now() + 10 * MINUTE));
    return token;
}

function isAuthenticated(req, db) {
    const token = req.get('X-Auth-Token');
    if (!token) return false;
    try {
        JWT.verify(token, db.metaData.secretKey);
        return true;
    } catch (x) {
        return false;
    }
}

function login(req, db) {
    const data = req.body;
    const user = findUser(data.username, db);
    if (!user) return null;
    if (data.password !== user.password) return null;

    const token = mkToken(user.username, db);
    return {
        token: token.compact(),
        username: user.username,
        date: new Date().toLocaleString()
    };
}

function isLoginReq(req) {
    return req.method === 'POST' && req.originalUrl === '/auth/login';
}

function isAuthRequired(req) {
    const methods = ['POST', 'PUT', 'DELETE'];
    const prefixes = ['/metaData', '/products']
    const M = req.method;
    const U = req.originalUrl;
    return methods.includes(M) || (M === 'GET' && prefixes.some(p => U.startsWith(p)));
}


const db = loadDb('db.json');
module.exports = (request, response, next) => {
    if (isLoginReq(request)) {
        const data = login(request, db);
        console.log('data: ', data)
        if (!!data) {
            response.status(201).send(data);
        } else {
            response.sendStatus(401);
        }
        return;
    }

    if (isAuthRequired(request)) {
        if (isAuthenticated(request, db)) {
            next();
        } else {
            response.sendStatus(401);
        }
        return;
    }

    next();
};
