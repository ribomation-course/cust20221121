import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../../services/products.service";
import {Observable} from "rxjs";
import {Product} from "../../domain/product";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.css']
})
export class ListPage implements OnInit {
    products$: Observable<Product[]>;

    constructor(private productSvc: ProductsService, private titleSvc: Title, private route: ActivatedRoute) {
        this.products$ = productSvc.findAll();
    }

    ngOnInit(): void {
        const title = this.route.snapshot.data['title'];
        this.titleSvc.setTitle(title);
    }

}
