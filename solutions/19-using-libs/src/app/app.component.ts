import {Component, OnInit} from '@angular/core';
import {DateTime} from 'luxon'


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    dates: DateTime[] = [];

    ngOnInit(): void {
        const now = DateTime.now();
        const offset = 5;
        const units = ['seconds', 'minutes', 'hours', 'days'];
        this.dates = [now]
            .concat(units
                .map(unit => {
                    const delta: any = {};
                    delta[unit] = offset;
                    return [now.minus(delta), now.plus(delta)];
                }).flat()
            ).sort();
    }
}
