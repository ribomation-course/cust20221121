import {Component} from '@angular/core';
import {Person} from "./person";
import {PERSONS} from "./person-list";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    persons: Person[] = PERSONS;

    toggle() {
        if (this.persons.length > 0) {
            this.persons = [];
        } else {
            this.persons = PERSONS;
        }
    }
}
