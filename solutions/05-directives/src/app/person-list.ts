import {Person} from "./person";

export const PERSONS: Person[] = [
    {
        "first_name": "Eleonora",
        "last_name": "Dana",
        "email": "edana0@issuu.com"
    }, {
        "first_name": "Joyous",
        "last_name": "O'Corrigane",
        "email": "jocorrigane1@hubpages.com"
    }, {
        "first_name": "Niki",
        "last_name": "Driver",
        "email": "ndriver2@ebay.co.uk"
    }, {
        "first_name": "Mohammed",
        "last_name": "Kausche",
        "email": "mkausche3@biblegateway.com"
    }, {
        "first_name": "Marvin",
        "last_name": "Tardiff",
        "email": "mtardiff4@qq.com"
    }, {
        "first_name": "Delia",
        "last_name": "Evesque",
        "email": "devesque5@furl.net"
    }, {
        "first_name": "Justus",
        "last_name": "Shakespear",
        "email": "jshakespear6@dropbox.com"
    }, {
        "first_name": "Teresa",
        "last_name": "Arro",
        "email": "tarro7@yelp.com"
    }, {
        "first_name": "Allsun",
        "last_name": "Sarah",
        "email": "asarah8@elpais.com"
    }, {
        "first_name": "Felisha",
        "last_name": "Mellon",
        "email": "fmellon9@histats.com"
    }
];