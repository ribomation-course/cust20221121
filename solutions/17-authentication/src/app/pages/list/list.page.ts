import {Component} from '@angular/core';
import {ProductsService} from "../../services/products.service";
import {Observable} from "rxjs";
import {Product} from "../../domain/product";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.css']
})
export class ListPage {
    products$: Observable<Product[]>;

    constructor(private productSvc: ProductsService,
                private router: Router,
                private authSvc: AuthService
    ) {
        this.products$ = productSvc.findAll();
    }

    async showProduct(id: number) {
        await this.router.navigate(['/show', id]);
    }

    async onLogout() {
        this.authSvc.logout();
        await this.router.navigate(['home']);
    }
}
