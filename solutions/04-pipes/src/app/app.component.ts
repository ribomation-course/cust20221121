import {Component} from '@angular/core';
import {Product} from "./domain/product";
import {PRODUCTS} from "./domain/product-list";

function sortPrice(asc: boolean) {
    return (lhs: Product, rhs: Product) => asc ? lhs.price - rhs.price : rhs.price - lhs.price;
}

function clone(lst: Product[]): Product[] {
    return lst.map(p => Object.assign({}, p));
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    readonly DAY = 24 * 3600 * 1000;
    asc: boolean = true;

    products: Product[] = clone(PRODUCTS);

    sort() {
        this.products = this.products.sort(sortPrice(this.asc));
        this.asc = !this.asc;
    }

    reset() {
        this.products = clone(PRODUCTS);
    }
}
