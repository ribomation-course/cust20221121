import {Product} from "./product";

const DAY = 24 * 3600 * 1000;
let nextSvc = false;
let offset = -45;

function mkCount(): number {
    return Math.random() * 10;
}

function mkPrice(): number {
    return Math.random() * 2000;
}

function mkService(): boolean {
    nextSvc = !nextSvc;
    return nextSvc;
}

function mkDate(): Date {
    const ts = Date.now() + offset * DAY;
    offset += 10;
    return new Date(ts);
}

export const PRODUCTS: Product[] = [
    {
        name: 'Apple Pie', price: mkPrice(), count: mkCount(), service: mkService(), updated: mkDate()
    },
    {
        name: 'Banana Split', price: mkPrice(), count: mkCount(), service: mkService(), updated: mkDate()
    },
    {
        name: 'Red Orange', price: mkPrice(), count: mkCount(), service: mkService(), updated: mkDate()
    },
    {
        name: 'Pine Apple', price: mkPrice(), count: mkCount(), service: mkService(), updated: mkDate()
    },
    {
        name: 'Coco Nut Ice Cream', price: mkPrice(), count: mkCount(), service: mkService(), updated: mkDate()
    }
];
