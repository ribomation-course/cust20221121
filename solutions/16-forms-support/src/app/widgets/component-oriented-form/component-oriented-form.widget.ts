import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../user.domain";

@Component({
    selector: 'component-oriented-form',
    templateUrl: './component-oriented-form.widget.html',
    styleUrls: ['./component-oriented-form.widget.css']
})
export class ComponentOrientedFormWidget implements OnInit {
    @Input('init') model: User = {name: '', age: 0,};
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<any>();

    nameCtrl = new FormControl('', [Validators.required, Validators.minLength(3)]);
    ageCtrl = new FormControl(0, [Validators.required, Validators.min(18), Validators.max(45)]);
    form: FormGroup = new FormGroup({
        name: this.nameCtrl,
        age: this.ageCtrl
    })

    ngOnInit(): void {
        if (!this.model) {
            throw new Error('missing user model object');
        }
        this.form.setValue(this.model);
    }

    submit() {
        const val = this.form.value;
        console.log('[component] %o', val);
        this.submitEmitter.emit(val);
        this.form?.reset();
        this.model = val;
        this.form.setValue(val);
    }

    cancel() {
        this.cancelEmitter.emit()
    }

}
