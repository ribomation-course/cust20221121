import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>User</h1>

    <label>
      <strong>Use Elvis Operator:</strong>
      <input type="checkbox" [(ngModel)]="useElvis">
    </label>

    <p>
      <strong>Name:</strong>
      <span *ngIf="useElvis">[{{user?.name?.first}} {{user?.name?.last}}]</span>
      <span *ngIf="!useElvis">[{{user.name.first}} {{user.name.last}}]</span>
    </p>
    <button (click)="toggle()">Update User</button>
  `,
  styles: [`
    :host {
      font-family: sans-serif;
      font-size: 1.5rem;
    }
  `]
})
export class AppComponent {
  user: any = {
    name: {
      first: 'Anna', last: 'Conda'
    }
  }

  useElvis: boolean = false;

  toggle() {
    if (this.user) {
      this.user = null;
    } else {
      this.user = {
        name: {
          first: 'Per', last: 'Silja'
        }
      }
    }
  }
}
