export interface Product {
    name: string;
    price: number;
    inStock: boolean;
}
