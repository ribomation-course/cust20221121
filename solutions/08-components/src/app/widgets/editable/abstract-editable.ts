import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Property, PropertyType} from "./property";

function isAbsent(obj: any) {
    return obj === undefined || obj === null;
}

@Component({template: ''})
export class AbstractEditable<T extends PropertyType> implements OnInit {
    editing: boolean = false;
    editableValue: PropertyType = this.defaultValue;

    @Input('value') propertyValue: PropertyType = this.defaultValue;
    @Input('name') propertyName: string = '';
    @Input('hint') hint: string = 'Click to edit';
    @Output('updated') updatedEmitter = new EventEmitter<Property>();

    ngOnInit(): void {
        if (isAbsent(this.propertyName)) {
            throw new Error('[editable-text] missing property name')
        }
    }

    get fieldType(): string {
        return 'text';
    }

    get defaultValue(): PropertyType {
        return '';
    }

    get empty(): boolean {
        const val: any = this.propertyValue;

        if (isAbsent(val)) return true;
        if (val instanceof Boolean) {
            return (val as boolean);
        }
        if (val instanceof Number) {
            return (val as number).toString().trim().length === 0;
        }
        if (val instanceof String) {
            return (val as string).trim().length === 0;
        }

        return false;
    }

    onKeyUp(evt: any) {
        if (evt.key === 'Escape') {
            this.revert();
        } else if (evt.key === 'Enter') {
            this.save();
        }
    }

    onChange(evt: any) {
        this.save();
    }

    edit() {
        this.editing = true;
        this.editableValue = this.propertyValue;
    }

    revert() {
        this.editing = false;
        this.editableValue = undefined;
    }

    save() {
        this.editing = false;
        this.propertyValue = this.editableValue;
        this.editableValue = undefined;
        const payload: Property = {
            name: this.propertyName as string,
            value: this.propertyValue as T,
        };
        this.updatedEmitter.emit(payload);
    }

}
