export type PropertyType = string | number | boolean | undefined;

export interface Property {
    name: string;
    value:  PropertyType;
}
