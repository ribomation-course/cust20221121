import {Component} from '@angular/core';
import {AbstractEditable} from "../abstract-editable";
import {PropertyType} from "../property";

@Component({
    selector: 'editable-number',
    templateUrl: '../abstract-editable.html',
    styleUrls: ['../abstract-editable.css']
})
export class EditableNumberWidget extends AbstractEditable<number> {
    override get fieldType():string {
        return 'number';
    }

    override get defaultValue(): PropertyType {
        return 0;
    }
}
