import {Component} from '@angular/core';
import {AbstractEditable} from "../abstract-editable";
import {PropertyType} from "../property";

@Component({
    selector: 'editable-boolean',
    templateUrl: '../abstract-editable.html',
    styleUrls: ['../abstract-editable.css']
})
export class EditableBooleanWidget extends AbstractEditable<number> {
    override get fieldType():string {
        return 'checkbox';
    }

    override get defaultValue(): PropertyType {
        return false;
    }
}
