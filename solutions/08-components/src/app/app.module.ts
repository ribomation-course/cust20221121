import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";

import {AppComponent} from './app.component';
import {AbstractEditable} from "./widgets/editable/abstract-editable";
import {EditableTextWidget} from "./widgets/editable/variants/editable-text.widget";
import {EditableNumberWidget} from "./widgets/editable/variants/editable-number.widget";
import {EditableBooleanWidget} from "./widgets/editable/variants/editable-boolean.widget";

@NgModule({
    declarations: [
        AppComponent,
        AbstractEditable,
        EditableTextWidget,
        EditableNumberWidget,
        EditableBooleanWidget,
    ],
    imports: [
        BrowserModule,
        FormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
