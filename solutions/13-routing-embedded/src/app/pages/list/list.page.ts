import {Component} from '@angular/core';
import {ProductsService} from "../../services/products.service";
import {Observable} from "rxjs";
import {Product} from "../../domain/product";
import {Router} from "@angular/router";

@Component({
    selector: 'app-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.css']
})
export class ListPage {
    products$: Observable<Product[]>;

    constructor(private productSvc: ProductsService, private router: Router) {
        this.products$ = productSvc.findAll();
    }

    async showProduct(id: number) {
        await this.router.navigate(['/show', id]);
    }
}
