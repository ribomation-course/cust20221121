import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {MockProductService, ProductService} from "./services/product.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: ProductService, useClass: MockProductService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
