import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LoggerService {
    verbose: boolean = true;

    print(msg: string) {
        if (this.verbose) {
            const ts = new Date().toLocaleTimeString();
            console.log('[%s] %s', ts, msg);
        }
    }
}
