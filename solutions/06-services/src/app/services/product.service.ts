import {Injectable} from '@angular/core';
import {Product} from "../domain/product";
import {Observable, of} from "rxjs";
import {LoggerService} from "./logger.service";

@Injectable({providedIn: 'root'})
export class ProductService {
    repo: Product[] = [
        {
            "id": 1,
            "name": "Escalade ESV",
            "price": 2009.74
        }, {
            "id": 2,
            "name": "Daewoo Magnus",
            "price": 1981.65
        }, {
            "id": 3,
            "name": "Galant",
            "price": 1897.19
        }, {
            "id": 4,
            "name": "Sorento",
            "price": 1852.81
        }, {
            "id": 5,
            "name": "Fifth Ave",
            "price": 2025.73
        }, {
            "id": 6,
            "name": "Grand Marquis",
            "price": 2003.81
        }, {
            "id": 7,
            "name": "G-Class",
            "price": 1815.49
        }, {
            "id": 8,
            "name": "Galant",
            "price": 1952.34
        }, {
            "id": 9,
            "name": "P-1800",
            "price": 1895.25
        }, {
            "id": 10,
            "name": "Amazon",
            "price": 1835.33
        }];

    constructor(log: LoggerService) {
        if (this instanceof MockProductService) {
            // log.print('svc super class');
        } else {
            log.print('ProductService created');
        }
    }

    findAll(): Observable<Product[]> {
        return of(this.repo);
    }
}

@Injectable()
export class MockProductService extends ProductService {
    constructor(log: LoggerService) {
        super(log);
        log.print('MockProductService created')
    }

    override findAll(): Observable<Product[]> {
        return of([
            {id: 100, name: 'Apple', price: 100.5},
            {id: 200, name: 'Banana', price: 250.25},
        ]);
    }
}
