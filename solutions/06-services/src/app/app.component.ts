import {Component} from '@angular/core';
import {LoggerService} from "./services/logger.service";
import {ProductService} from "./services/product.service";
import {Product} from "./domain/product";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    products: Product[] = [];
    cnt: number = 1;

    constructor(private logger: LoggerService, public productSvc: ProductService) {
        logger.print('App started')
        productSvc.findAll().subscribe(lst => this.products = lst)
    }

    msg() {
        this.logger.print('Greeting #' + this.cnt++);
    }
}
