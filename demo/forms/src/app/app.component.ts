import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
    <model-driven-form></model-driven-form>
    <reactive-form></reactive-form>
    <template-driven-form></template-driven-form>
  `,
  styles: []
})
export class AppComponent {
  title = 'Forms Demo';
}
