import {Component, OnInit, ViewChild} from "@angular/core";

export class ProductModel {
  constructor(public name : string = "",
              public price: number = 0) {
  }
  reset(): void {
    this.name = "";
    this.price = 0;
  }
}

@Component({
  selector: "template-driven-form",
  template: `
    <div>
      <h2>Template-Drive Form</h2>
      <form novalidate #form="ngForm" (ngSubmit)="onSubmit()">
        <table>
          <tr><th>Product Name</th>
            <td><input type="text" required minlength="5"
                       name="productName" [(ngModel)]="model.name"/></td>
          </tr>
          <tr><th>Price</th>
            <td><input type="number" required
                       name="price" [(ngModel)]="model.price" min="0"/></td>
          </tr>
          <tr><th></th>
            <td>
              <button type="submit" [disabled]="form.invalid">SEND</button>
              <button type="button" (click)="onReset()">Clear</button>
            </td>
          </tr>
        </table>
      </form>
      <pre>DATA: {{form.value | json}}</pre>
    </div>
  `,
})
export class TemplateDrivenFormComponent {
  @ViewChild("form") form: any;
  model: ProductModel = new ProductModel();

  onSubmit(): void {
    if (this.form.valid) {
      console.log("[model-driven]", "form data", this.form.value);
      this.onReset();
    } else {
      console.log("[model-driven]", "form data invalid");
    }
  }

  onReset(): void {
    this.model.reset();
  }
}
