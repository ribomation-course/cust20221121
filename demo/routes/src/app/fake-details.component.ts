import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
@Component({
  selector: "app-fake-details",
  template: `
    <div>Now showing item: {{name | async}}</div>
  `,
})
export class FakeDetailsComponent {
  name: Observable<string>;
  constructor(private route: ActivatedRoute) {
    this.name = route.params.map(p => p.name);
  }
}

