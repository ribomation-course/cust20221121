import {Component} from "@angular/core";

@Component({
  selector: "app-first",
  template: `<p>This is First</p>`,
})
export class FirstComponent {}
