import {Injectable} from "@angular/core";

@Injectable()
export abstract class FibonacciService {
  abstract compute(n: number): number;
}

export class RecursiveFibonacci extends FibonacciService {
  compute(n: number): number {
    return this.fib(n);
  }
  private fib(n: number): number {
    return n <= 2 ? 1 : this.fib(n - 2) + this.fib(n - 1);
  }
}

export class IterativeFibonacci extends FibonacciService {
  compute(n: number): number {
    let fn2 = 1, fn1 = 1;
    while (n-- > 2) {
      let f = fn2 + fn1;
      fn2 = fn1;
      fn1 = f;
    }
    return fn1;
  }
}

