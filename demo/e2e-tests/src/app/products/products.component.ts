import {Component, OnInit} from '@angular/core';
import {ProductDaoService} from '../product-dao/product-dao.service';
import {Product} from '../model/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];

  constructor(private productDao: ProductDaoService) {
  }

  ngOnInit() {
    this.products = this.productDao.products;
  }

}
