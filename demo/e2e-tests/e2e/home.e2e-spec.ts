import { HomePage } from './pages/home.page';

describe('Page: Home', () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
    page.navigateTo();
  });

  it('should display welcome message', () => {
    expect(page.getHeaderText()).toContain('tst');
  });
});

