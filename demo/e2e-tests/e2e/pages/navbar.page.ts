import {browser, by, element, ElementFinder} from 'protractor';

export class NavbarPage {
  navigateTo() {
    return browser.get('/home');
  }

  getHomeLink(): ElementFinder {
    return element(by.linkText('Home'));
  }

  getProductsLink(): ElementFinder {
    return element(by.linkText('Products'));
  }

  getAboutLink(): ElementFinder {
    return element(by.linkText('About'));
  }


}
