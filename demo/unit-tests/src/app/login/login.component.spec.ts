import {ComponentFixture, TestBed} from '@angular/core/testing';
import {LoginComponent, User} from './login.component';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('LoginComponent', () => {
  let component : LoginComponent;
  let fixture   : ComponentFixture<LoginComponent>;
  let submitElem: DebugElement;
  let emailElem : DebugElement;
  let pwdElem   : DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent]
    });
    fixture    = TestBed.createComponent(LoginComponent);
    component  = fixture.componentInstance;
    submitElem = fixture.debugElement.query(By.css('#login'));
    emailElem  = fixture.debugElement.query(By.css('input[type=email]'));
    pwdElem    = fixture.debugElement.query(By.css('input[type=password]'));
    fixture.detectChanges();
  });

  it('should toggle button disable', () => {
    component.valid = false;
    expect(submitElem.nativeElement.disabled).toBeTruthy();

    component.valid = !component.valid;
    fixture.detectChanges();
    expect(submitElem.nativeElement.disabled).toBeFalsy();
  });

  it('should emit user object after pressing login button', () => {
    component.valid = true;
    emailElem.nativeElement.value = 'anna.conda@foobar.com';
    pwdElem.nativeElement.value = 'secret';

    let user: User;
    component.fireLoggedIn.subscribe(data => user = data);
    expect(user).toBeUndefined();

    submitElem.triggerEventHandler('click', new Event('click'));
    expect(user).toBeDefined();
    expect(user.email).toBe('anna.conda@foobar.com');
    expect(user.password).toBe('secret');
  });

});

