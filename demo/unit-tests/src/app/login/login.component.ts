import {Component, EventEmitter, Input, Output} from '@angular/core';

export class User {
  constructor(public email: string, public password: string) {}
}

@Component({
  selector: 'app-login',
  template: `
    <form>
      <label  for="email"></label> <input type="email" #email/>
      <label  for="pwd"></label>   <input type="password" #pwd/>
      <button id="login" type="button" (click)="login(email.value, pwd.value)" [disabled]="!valid">Login</button>
    </form>
  `,
  styles: []
})
export class LoginComponent {
  @Input()  valid = false;
  @Output() fireLoggedIn = new EventEmitter<User>();

  login(email: string, password: string): void {
    if (email && password) {
      this.fireLoggedIn.emit(new User(email, password));
    }
  }
}
