import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-number-stepper',
  template: `
    <div>
      <button id="inc" (click)="increment()">[+]</button>
      <span id="val">{{value}}</span> <span id="msg">{{message}}</span>
      <button id="dec" (click)="decrement()">[-]</button>
    </div>
  `,
  styles: []
})
export class NumberStepperComponent implements OnInit {
  static readonly MAX = 10;
  static readonly MIN = 1;
  static DELAY = 5 * 1000;
  value: number = 5;
  message: string;

  increment(): void {
    if (this.value < NumberStepperComponent.MAX) {
      this.delayedUpdate(this.value + 1);
    } else {
      this.delayedUpdate(NumberStepperComponent.MAX, 'OVERFLOW');
    }
  }

  decrement(): void {
    if (this.value > NumberStepperComponent.MIN) {
      this.delayedUpdate(this.value - 1);
    } else {
      this.delayedUpdate(NumberStepperComponent.MIN, 'UNDERFLOW');
    }
  }

  delayedUpdate(val: number = 5, msg: string = undefined) {
    setTimeout(() => {
      this.value = val;
      this.message = msg;
    }, NumberStepperComponent.DELAY);
  }

  ngOnInit() { this.delayedUpdate(); }
}

