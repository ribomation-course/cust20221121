import {async, ComponentFixture, fakeAsync, TestBed, tick, flush} from '@angular/core/testing';

import {NumberStepperComponent} from './number-stepper.component';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('NumberStepperComponent', () => {
  let component: NumberStepperComponent;
  let fixture: ComponentFixture<NumberStepperComponent>;
  let incEl: DebugElement;
  let decEl: DebugElement;
  let valEl: DebugElement;
  let msgEl: DebugElement;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [NumberStepperComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberStepperComponent);
    component = fixture.componentInstance;
    incEl = fixture.debugElement.query(By.css('#inc'));
    decEl = fixture.debugElement.query(By.css('#dec'));
    valEl = fixture.debugElement.query(By.css('#val'));
    msgEl = fixture.debugElement.query(By.css('#msg'));
    fixture.detectChanges();
  });

  it('should create actors', () => {
    expect(component).toBeDefined();
    expect(fixture).toBeDefined();
    expect(incEl).toBeDefined();
    expect(decEl).toBeDefined();
    expect(valEl).toBeDefined();
    expect(msgEl).toBeDefined();
  });

  let value = () => {
    fixture.detectChanges();
    return parseInt(valEl.nativeElement.innerText);
  };
  let msg = () => {
    fixture.detectChanges();
    return msgEl.nativeElement.innerText;
  };

  it('should show incremented value after N secs', fakeAsync(() => {
    NumberStepperComponent.DELAY = 3 * 1000;
    const INITIAL_VALUE = 5;
    component.value = INITIAL_VALUE;

    expect(value()).toEqual(INITIAL_VALUE);
    incEl.triggerEventHandler('click', null);

    tick(1000);
    expect(value()).toEqual(INITIAL_VALUE);

    tick(1000);
    expect(value()).toEqual(INITIAL_VALUE);

    tick(1000);
    expect(value()).toEqual(INITIAL_VALUE + 1);
    expect(msg()).toBe('');
  }));

  it('should show max value and message at overflow', fakeAsync(() => {
    NumberStepperComponent.DELAY = 2 * 1000;
    const INITIAL_VALUE = NumberStepperComponent.MAX;
    component.value = INITIAL_VALUE;

    expect(value()).toEqual(INITIAL_VALUE);
    expect(msg()).toBe('');
    incEl.triggerEventHandler('click', null);

    tick(NumberStepperComponent.DELAY + 100);
    expect(value()).toEqual(INITIAL_VALUE);
    expect(msg()).toBe('OVERFLOW');
  }));

  it('should be possible to test an arbitrary amount of time', fakeAsync(() => {
    NumberStepperComponent.DELAY =  Math.ceil(25 * Math.random()) * 1000;
    const INITIAL_VALUE = 5;
    component.value = INITIAL_VALUE;

    expect(value()).toEqual(INITIAL_VALUE);
    incEl.triggerEventHandler('click', null);

    let passedTime = flush();
    expect(value()).toEqual(INITIAL_VALUE + 1);
    console.info('passed time',  Math.ceil(passedTime/1000), 'secs');
  }));

});

